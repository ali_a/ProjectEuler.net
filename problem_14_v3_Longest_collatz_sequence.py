'''
this code was written by projecteuler.net community members,
not me, definitely not me :(
https://projecteuler.net/overview=014
'''

def count_chain(n):
	if n in values:
		return values[n]
	if (n % 2 == 0):
		values[n] = 1 + count_chain(n/2)
	else:
		values[n] = 2 + count_chain((n * 3 + 1) / 2)
	return values[n]

longest_chain = 0
answer = -1
values = {1:1}

for number in range(500000, 1000000-1):
	if count_chain(number) > longest_chain:
		longest_chain = count_chain(number)
		answer = number

print(str(answer))
