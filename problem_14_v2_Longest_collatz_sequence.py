longest_chain_size = 0
longest_chain_number = 0

for i in range(2, 1000000):
	number = i
	chain_size = 1
	while(i > 1):
		if(i % 2 == 0):
			i = i / 2
			chain_size += 1
		else:
			i = (i * 3 + 1) / 2
			chain_size += 2

	if (chain_size > longest_chain_size):
		longest_chain_size = chain_size
		longest_chain_number = number

print(str(longest_chain_number))

'''
Answer:
	837799
Completed on Sun, 7 Oct 2018
'''
