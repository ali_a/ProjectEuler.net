'''
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
	a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
'''
import time

# brute force approch:
# took about 52.7 seconds
def brute_force():
    start = time.time()
    for c in range(2,1000):
        for b in range(1,1000):
            for a in range(1,1000):
                if( (b < c) and (a <= b) and (a**2 + b**2 == c**2) and (a+b+c == 1000) ):
                    print('a = ', a)
                    print('b = ', b)
                    print('c = ', c)
                    print('abc = ', a*b*c)
    stop = time.time()
    print('brute force finished in ', stop - start)

# brute force v2:
# took about 34 seconds (35% faster than v1)
def brute_force_v2():
    start = time.time()
    for c in range(2,1000):
        for b in range(1,1000):
            if (b >= c):
                break
            for a in range(1,1000):
                if( (a <= b) and (a**2 + b**2 == c**2) and (a+b+c == 1000) ):
                    print('a = ', a)
                    print('b = ', b)
                    print('c = ', c)
                    print('abc = ', a*b*c)
    stop = time.time()
    print('brute force v2 finished in ', stop - start)

# brute force v3:
# took about 3.9 seconds (92.5% faster than v1) (88.5% faster than v2)
def brute_force_v3():
    start = time.time()
    for c in range(2,1000):
        for b in range(1,1000):
            if (b >= c):
                break
            for a in range(1,1000):
                if( (a <= b) and (a**2 + b**2 == c**2) and (a+b+c == 1000) ):
                    print('a = ', a)
                    print('b = ', b)
                    print('c = ', c)
                    print('abc = ', a*b*c)
                    stop = time.time()
                    print('brute force v3 finished in ', stop - start)
                    return

# brute force v4:
# took about 0.029 seconds :D
def brute_force_v4():
    start = time.time()
    for b in range(1,1000):
        for a in range(b,1000):
            c = 1000 - a - b
            if(a**2 + b**2 == c**2):
                print('a = ', a)
                print('b = ', b)
                print('c = ', c)
                print('abc = ', a*b*c)
                stop = time.time()
                print('brute force v4 finished in ', stop - start)
                return

brute_force_v4()
