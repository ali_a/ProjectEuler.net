/*
Took 48s
*/

#include <stdio.h>

long int brute_force(int start, int stop){
    long int answer = 1;
    long int i;
    while(1){
        _Bool is_answer =  1;
        answer += 1;
        //printf("%lu\n", answer);
        for(i = start; i < stop+1; i++){
            //printf("checking : %lu\n", i);
            if(answer % i != 0){
                is_answer = 0;
            }
        }
        if(is_answer == 1){
            return answer;
        }
    }
}

int main(void){
    printf("%lu\n", brute_force(1, 20));
    return 1;
}
