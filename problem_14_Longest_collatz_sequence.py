# -*- coding: utf-8 -*-


count_chains_long = []

for i in range(1, 1000000):
	i = i + 1
	current_chain = []
	n = i
	current_chain.append(n)
	while(n > 1):
		if(n % 2 == 0):
			n = n / 2
		else:
			n = n * 3 + 1
		current_chain.append(n)
	#print(current_chain, len(current_chain))
	count_chains_long.append(len(current_chain))
	#print(str(i), " Done!")
print(count_chains_long.index(max(count_chains_long)) + 2)
