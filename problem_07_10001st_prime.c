#include <stdio.h>
 _Bool is_prime(int number)
{
         int loop_counter;
         for(loop_counter = 2; loop_counter <= number/2+1; loop_counter++){
                  if(number % loop_counter == 0){
                        return 0;
                  }
         }
         return 1;
}
int main(void)
{
        int prime_number = 10001;
        int loop_counter = 0;
        int next_prime = 1;
        while(loop_counter != prime_number){
                 next_prime++;
                 if(is_prime(next_prime))
                        loop_counter++;
        }
        printf("%d", next_prime);
}
// took ~ 0.85 seconds!