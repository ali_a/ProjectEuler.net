'''
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
'''
import time

def is_prime(x):
    for i in range(2,x/2+1):
        if x % i == 0:
            return False
    return True

def is_prime_v2(x):
    for i in range(2,int(x**0.5)+1):
        if x % i == 0:
            return False
    return True

# only odd numbers
def is_prime_v3(x):
    for i in range(3, int(x**0.5)+1, 2):
        if x % i == 0:
            return False
        return True

# took a lot with is_prime() as the prime_function, breaked meanwhile it was working
# took about 20 seconds with is_prime_v2() as the prime_function
# took about 6 seconds with is_prime_v3() as the prime_function and 2 as init_sum_value
def sum_of_primes(x, prime_function, init_sum_value = 0):
    start = time.time()
    sum = init_sum_value
    for i in range(2,x):
        if prime_function(i):
            sum += i
    print sum
    stop = time.time()
    print('finished in ', stop - start)

sum_of_primes(2000000, is_prime_v3, 2)
